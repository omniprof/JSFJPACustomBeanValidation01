package com.kfwebstandard.entities;

import com.kfwebstandard.jsr303validator.OptionalCellNumber;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

/**
 * This entity class has had the attribute 'message' added to the bean
 * validation annotations so that custom and localized messages can be used. The
 * messages can be found in ValidationMessages.properties that must be placed in
 * the root of the classpath. This means into the src/main/resources folder
 * without any package
 *
 * @author Ken
 */
@Entity
@Table(name = "client", catalog = "JSF_COMPANY", schema = "")
@NamedQueries({
    @NamedQuery(name = "Client.findAll", query = "SELECT c FROM Client c"),
    @NamedQuery(name = "Client.findByEmailAndPassword", query = "select c from Client c where c.email=?1 and c.password=?2"),
    @NamedQuery(name = "Client.findByEmail", query = "SELECT c FROM Client c WHERE c.email = :email"),})
public class Client implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "CLIENT_NUMBER")
    private Integer clientNumber;

    @Basic(optional = false)
    @NotNull(message = "{blank}")
    @Size(min = 0, max = 15, message = "{length}")
    @Column(name = "TITLE")
    private String title;

    @Basic(optional = false)
    @NotNull(message = "{blank}")
    @Size(min = 1, max = 30, message = "{length}")
    @Column(name = "LAST_NAME")
    private String lastName;

    @Basic(optional = false)
    @NotNull(message = "{blank}")
    @Size(min = 1, max = 30, message = "{length}")
    @Column(name = "FIRST_NAME")
    private String firstName;

    @Basic(optional = false)
    @NotNull(message = "{blank}")
    @Size(min = 0, max = 45, message = "{length}")
    @Column(name = "COMPANY_NAME")
    private String companyName;

    @Basic(optional = false)
    @NotNull(message = "{blank}")
    @Size(min = 1, max = 45, message = "{length}")
    @Column(name = "ADDRESS_1")
    private String address1;

    @Basic(optional = false)
    @NotNull(message = "{blank}")
    @Size(min = 0, max = 45, message = "{length}")
    @Column(name = "ADDRESS_2")
    private String address2;

    @Basic(optional = false)
    @NotNull(message = "{blank}")
    @Size(min = 1, max = 30, message = "{length}")
    @Column(name = "CITY")
    private String city;

    @Basic(optional = false)
    @NotNull(message = "{blank}")
    @Size(min = 1, max = 25, message = "{length}")
    @Column(name = "PROVINCE")
    private String province;

    @Basic(optional = false)
    @NotNull(message = "{blank}")
    @Size(min = 1, max = 30, message = "{length}")
    @Column(name = "COUNTRY")
    private String country;

    @Basic(optional = false)
    @NotNull(message = "{blank}")
    @Size(min = 1, max = 7, message = "{length}")
    @Column(name = "POSTAL_CODE")
    private String postalCode;

    @Basic(optional = false)
    @NotNull(message = "{blank}")
    @Size(min = 14, max = 14, message = "{fixedlength}")
    @Column(name = "HOME_PHONE")
    private String homePhone;

    @Basic(optional = false)
    @NotNull(message = "{blank}")
    @Column(name = "CELL_PHONE")
    // Custom validator accepts a length of 0 or 14
    @OptionalCellNumber(message = "{fixedlength}")
    private String cellPhone;

    @Pattern(regexp = "[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?", message = "{invalidemail}")
    @Basic(optional = false)
    @NotNull(message = "{blank}")
    @Size(min = 1, max = 60, message = "{length}")
    @Column(name = "EMAIL")
    private String email;

    @Basic(optional = false)
    @NotNull(message = "{blank}")
    @Size(min = 2, max = 12, message = "{length}")
    @Column(name = "PASSWORD")
    private String password;

    @Basic(optional = false)
    //@NotNull
    @Column(name = "DATE_ENTERED", insertable = false, updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateEntered;

    public Client() {
    }

    public Client(Integer clientNumber) {
        this.clientNumber = clientNumber;
    }

    public Client(Integer clientNumber, String title, String lastName, String firstName, String companyName, String address1, String address2, String city, String province, String country, String postalCode, String homePhone, String cellPhone, String email, String password, Date dateEntered) {
        this.clientNumber = clientNumber;
        this.title = title;
        this.lastName = lastName;
        this.firstName = firstName;
        this.companyName = companyName;
        this.address1 = address1;
        this.address2 = address2;
        this.city = city;
        this.province = province;
        this.country = country;
        this.postalCode = postalCode;
        this.homePhone = homePhone;
        this.cellPhone = cellPhone;
        this.email = email;
        this.password = password;
        this.dateEntered = dateEntered;
    }

    public Integer getClientNumber() {
        return clientNumber;
    }

    public void setClientNumber(Integer clientNumber) {
        this.clientNumber = clientNumber;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getHomePhone() {
        return homePhone;
    }

    public void setHomePhone(String homePhone) {
        this.homePhone = homePhone;
    }

    public String getCellPhone() {
        return cellPhone;
    }

    public void setCellPhone(String cellPhone) {
        this.cellPhone = cellPhone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getDateEntered() {
        return dateEntered;
    }

    public void setDateEntered(Date dateEntered) {
        this.dateEntered = dateEntered;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (clientNumber != null ? clientNumber.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Client)) {
            return false;
        }
        Client other = (Client) object;
        if ((this.clientNumber == null && other.clientNumber != null) || (this.clientNumber != null && !this.clientNumber.equals(other.clientNumber))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.kfwebstandard.entities.Client[ clientNumber=" + clientNumber + " ]";
    }

}
