package com.kfwebstandard.formcontroller;

import com.kfwebstandard.jpa.controller.ClientJpaController;
import com.kfwebstandard.entities.Client;
import com.kfwebstandard.util.MessagesUtil;
import java.io.Serializable;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Controller class for login.xhtml
 *
 * @author Ken
 */
@Named
@ViewScoped
public class LoginController implements Serializable {

    private final static Logger LOG = LoggerFactory.getLogger(LoginController.class);

    @Inject
    private ClientJpaController clientJPAController;

    private String email;

    private String password;

    public String getEmail() {
        LOG.info(email);
        return email;
    }

    public void setEmail(String email) {
        LOG.info(email);
        this.email = email;
    }

    public String getPassword() {
        LOG.info(password);
        return password;
    }

    public void setPassword(String password) {
        LOG.info(password);
        this.password = password;

    }

    /**
     * Action
     */
    public void login() {

        // Get Session object so that the status of the individual who just logged in
        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);

        // Example of logging
        LOG.info("Entering login method email is {0} password is {1}", new Object[]{email, password});

        // Used to contain that will be displayed on the login form after Login button is pressed
        FacesMessage message;
        boolean loggedIn;

        // Is there a client with these credentials
        Client client = clientJPAController.findClient(email, password);

        // There is a client so login was successful
        if (client != null) {
            loggedIn = true;
            message = MessagesUtil.getMessage(
                    "com.kfwebstandard.bundles.messages", "welcome", new Object[]{email});
            message.setSeverity(FacesMessage.SEVERITY_INFO);
        } else {
            // Unsuccessful login
            loggedIn = false;
            // MessagesUtil simplifies creating localized messages
            message = MessagesUtil.getMessage(
                    "com.kfwebstandard.bundles.messages", "loginerror", new Object[]{email});
            message.setSeverity(FacesMessage.SEVERITY_ERROR);
        }
        // Store the outcome in the session object
        session.setAttribute("loggedIn", loggedIn);

        // Place the message in the context so that it will be displayed
        FacesContext.getCurrentInstance().addMessage(null, message);
    }
}
