package com.kfwebstandard.formcontroller;

import com.kfwebstandard.entities.Client;
import com.kfwebstandard.jpa.controller.ClientJpaController;
import com.kfwebstandard.util.MessagesUtil;
import java.io.Serializable;
import javax.faces.application.FacesMessage;

import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
// view rather than bean
import javax.faces.view.ViewScoped;

/**
 * Controller class for signup.xhtml
 *
 * @author Ken
 */
@Named("signupController")
@ViewScoped
public class SignupController implements Serializable {

    @Inject
    private ClientJpaController clientJPAController;

    private Client client;

    /**
     * Client created if it does not exist.
     *
     * @return
     */
    public Client getClient() {
        if (client == null) {
            client = new Client();
        }
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    /**
     * This method checks to see if the email address is already exists.
     *
     * @return A boolean value.
     */
    private boolean isValidEmail() {
        boolean valid = false;
        String email = client.getEmail();
        if (email != null) {
            if (clientJPAController.findClientByEmail(email) == null) {
                valid = true;
            } else {
                FacesMessage message = MessagesUtil.getMessage(
                        "com.kfwebstandard.bundles.messages", "email.in.use", null);
                message.setSeverity(FacesMessage.SEVERITY_ERROR);
                FacesContext.getCurrentInstance().addMessage("signupForm:email", message);
            }
        }
        return valid;
    }

    /**
     * Perform email check and if successful then move on to login.xhtml
     *
     * @return
     * @throws Exception
     */
    public String signup() throws Exception {
        if (isValidEmail()) {
            clientJPAController.create(client);
            return "login";
        }
        return null;
    }
}
