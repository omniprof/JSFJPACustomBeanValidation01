package com.kfwebstandard.jpa.controller;

import com.kfwebstandard.entities.Client;
import com.kfwebstandard.jpa.controller.exceptions.NonexistentEntityException;
import com.kfwebstandard.jpa.controller.exceptions.RollbackFailureException;
import java.io.Serializable;
import java.util.List;
import javax.annotation.Resource;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This is the modified NetBeans generated JPA controller
 *
 * @author kenfogel
 */
@Named
@RequestScoped
public class ClientJpaController implements Serializable {

    private final static Logger LOG = LoggerFactory.getLogger(ClientJpaController.class);

    @Resource
    private UserTransaction utx;

    @PersistenceContext(unitName = "jsfCompanyPU")
    private EntityManager em;

    public ClientJpaController() {
    }

    /**
     * Create a new record
     *
     * @param client
     * @throws RollbackFailureException
     */
    public void create(Client client) throws RollbackFailureException {
        try {
            utx.begin();
            em.persist(client);
            utx.commit();
        } catch (NotSupportedException | SystemException | RollbackException | HeuristicMixedException | HeuristicRollbackException | SecurityException | IllegalStateException ex) {
            try {
                utx.rollback();
                LOG.error("Rollback");
            } catch (IllegalStateException | SecurityException | SystemException re) {
                LOG.error("Rollback2");

                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
        }
    }

    /**
     * Update an existing record
     *
     * @param client
     * @throws NonexistentEntityException
     * @throws RollbackFailureException
     * @throws Exception
     */
    public void edit(Client client) throws NonexistentEntityException, RollbackFailureException, Exception {
        try {
            utx.begin();
            client = em.merge(client);
            utx.commit();
        } catch (IllegalStateException | SecurityException | HeuristicMixedException | HeuristicRollbackException | NotSupportedException | RollbackException | SystemException ex) {
            try {
                utx.rollback();
            } catch (IllegalStateException | SecurityException | SystemException re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = client.getClientNumber();
                if (findClient(id) == null) {
                    throw new NonexistentEntityException("The client with id " + id + " no longer exists.");
                }
            }
            throw ex;
        }
    }

    /**
     * Delete a record
     *
     * @param id
     * @throws NonexistentEntityException
     * @throws RollbackFailureException
     * @throws Exception
     */
    public void destroy(Integer id) throws NonexistentEntityException, RollbackFailureException, Exception {
        try {
            utx.begin();
            Client client;
            try {
                client = em.getReference(Client.class, id);
                client.getClientNumber();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The client with id " + id + " no longer exists.", enfe);
            }
            em.remove(client);
            utx.commit();
        } catch (NonexistentEntityException | IllegalStateException | SecurityException | HeuristicMixedException | HeuristicRollbackException | NotSupportedException | RollbackException | SystemException ex) {
            try {
                utx.rollback();
            } catch (IllegalStateException | SecurityException | SystemException re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        }
    }

    /**
     * Return all records
     *
     * @return
     */
    public List<Client> findClientEntities() {
        return findClientEntities(true, -1, -1);
    }

    /**
     * Return all records between the specified record numbers
     *
     * @param maxResults
     * @param firstResult
     * @return
     */
    public List<Client> findClientEntities(int maxResults, int firstResult) {
        return findClientEntities(false, maxResults, firstResult);
    }

    private List<Client> findClientEntities(boolean all, int maxResults, int firstResult) {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        cq.select(cq.from(Client.class));
        Query q = em.createQuery(cq);
        if (!all) {
            q.setMaxResults(maxResults);
            q.setFirstResult(firstResult);
        }
        return q.getResultList();
    }

    /**
     * Return a record based on the primary key
     *
     * @param id
     * @return
     */
    public Client findClient(Integer id) {
        return em.find(Client.class, id);
    }

    /**
     * Find a record with the specified email and password
     *
     * @param email
     * @param password
     * @return
     */
    public Client findClient(String email, String password) {
        TypedQuery<Client> query = em.createNamedQuery("Client.findByEmailAndPassword", Client.class);
        query.setParameter(1, email);
        query.setParameter(2, password);
        List<Client> clients = query.getResultList();
        if (!clients.isEmpty()) {
            return clients.get(0);
        }
        return null;
    }

    /**
     * Find a record with the specified email
     *
     * @param email
     * @return
     */
    public Client findClientByEmail(String email) {
        TypedQuery<Client> query = em.createNamedQuery("Client.findByEmail", Client.class);
        query.setParameter("email", email);
        List<Client> clients = query.getResultList();
        if (!clients.isEmpty()) {
            return clients.get(0);
        }
        return null;
    }

    /**
     * Return the number of records
     *
     * @return
     */
    public int getClientCount() {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        Root<Client> rt = cq.from(Client.class);
        cq.select(em.getCriteriaBuilder().count(rt));
        Query q = em.createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }

}
