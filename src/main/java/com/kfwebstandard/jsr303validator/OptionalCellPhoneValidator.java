package com.kfwebstandard.jsr303validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 *
 * @author Ken
 */
public class OptionalCellPhoneValidator implements ConstraintValidator<OptionalCellNumber, String> {

//    @Override
//    public void initialize(OptionalCellNumber constraintAnnotation) {
//    }

    /*
     1.  If length of the string is 0 then pass validation
     2.  If the length of the string is not 14 fail validation
     */
    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {

        if (value.length() == 0) {
            return true;
        }

        return value.length() == 14;
    }
}
